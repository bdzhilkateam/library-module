from django.db import models
from django.urls import reverse


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    cat_name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='categories/', default='')

    def __str__(self):
        return self.cat_name

    def get_absolute_url(self):
        return reverse('category', kwargs={'cat_id': self.id})


class Film(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    release = models.CharField(max_length=255)
    content = models.TextField(blank=True)
    image = models.ImageField(upload_to='films/', default='')
    trailer = models.URLField(max_length=255, default='')
    category = models.ForeignKey(Category, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('film', kwargs={'film_id': self.id})


class Book(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    release = models.CharField(max_length=255)
    image = models.ImageField(upload_to='books/', default='')
    content = models.TextField(blank=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('book', kwargs={'book_id': self.id})


class Games(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    release = models.CharField(max_length=255)
    image = models.ImageField(upload_to='games/', default='')
    content = models.TextField(blank=True)
    trailer = models.URLField(max_length=255, default='')
    category = models.ForeignKey(Category, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('game', kwargs={'game_id': self.id})


class Music(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)
    release = models.CharField(max_length=255)
    image = models.ImageField(upload_to='music/', default='')
    trailer = models.URLField(max_length=255, default='')
    content = models.TextField(blank=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('music', kwargs={'music_id': self.id})
