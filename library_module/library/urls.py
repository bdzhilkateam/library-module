from django.urls import path
from .views import *


urlpatterns = [
    path('categorylist/', MainPage.as_view(), name='main_page'),
    path('showbycategory/<int:cat_id>/', ShowByCategory.as_view(), name='category'),
    path('showfilm/<int:film_id>', ShowContentFilm.as_view(), name='film'),
    path('showbook/<int:book_id>', ShowContentBook.as_view(), name='book'),
    path('showmusic/<int:games_id>', ShowContentMusic.as_view(), name='games'),
    path('showmusic/<int:music_id>', ShowContentMusic.as_view(), name='music'),
]

