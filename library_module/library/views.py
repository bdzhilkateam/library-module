from django.views.generic import ListView

from .models import *


model_list = [Book, Film, Games, Music]


class MainPage(ListView):
    model = Category
    template_name = 'library/index.html'
    context_object_name = 'cats'

    def get_queryset(self):
        return Category.objects.all()


class ShowByCategory(ListView):
    template_name = 'library/show.html'
    context_object_name = 'posts'

    def get_queryset(self):
        for mod in model_list:
            if mod.objects.first().category_id == self.kwargs['cat_id']:
                return mod.objects.all()


class ShowContentFilm(ListView):
    model = Film
    template_name = 'library/show_content.html'
    context_object_name = 'contents'

    def get_queryset(self):
        return Film.objects.filter(id=self.kwargs['film_id'])


class ShowContentBook(ListView):
    model = Book
    template_name = 'library/show_content.html'
    context_object_name = 'contents'

    def get_queryset(self):
        return Book.objects.filter(id=self.kwargs['book_id'])


class ShowContentMusic(ListView):
    model = Music
    template_name = 'library/show_content.html'
    context_object_name = 'contents'

    def get_queryset(self):
        return Music.objects.filter(id=self.kwargs['music_id'])

