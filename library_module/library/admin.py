from django.contrib import admin

from .models import *

admin.site.register(Category)
admin.site.register(Film)
admin.site.register(Book)
admin.site.register(Games)
admin.site.register(Music)

