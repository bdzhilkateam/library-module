# Generated by Django 4.1.7 on 2023-04-11 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0002_book_image_category_image_film_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='image',
            field=models.ImageField(default='', upload_to='books/'),
        ),
        migrations.AlterField(
            model_name='category',
            name='image',
            field=models.ImageField(default='', upload_to='categories/'),
        ),
        migrations.AlterField(
            model_name='film',
            name='image',
            field=models.ImageField(default='', upload_to='films/'),
        ),
    ]
