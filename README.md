# Library module
## Running 
- pip install -r requirements.txt
- python manage.py runserver
## Endpoints
| HTTP METHOD | URL                           | Description               |
|-------------|-------------------------------|---------------------------|
| GET         | /categorylist                 | all category for learning |
| GET         | /showbycategory/<int:cat_id>/ | content by category       |
| GET         | /showfilm/<int:film_id>/      | content by film           |
| GET         | /showbook/<int:book_id>/      | content by book           |
| GET         | /showgames/<int:game_id>/     | content by game           |
| GET         | /showmusic/<int:music_id>/    | content by music          |